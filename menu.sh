#!/bin/bash

# menu.sh
#
# A script written by Ranko Kohime to provide a fancy menu.

# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
#set -Eeuxo pipefail

# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
# Get script directory for use in next section
SCRIPTPATH=$(dirname `readlink -f "$0"`)

sourceFuncs()
{
  # Source accessory functions
  # This is being done in a loop, as sourcing multiple files with globbing appears not to work.
  for i in `find $SCRIPTPATH -name "*.func"`;
    do
      source $i
  done
}

# Ensure easybashgui is in $PATH for script
PATH="$PATH:${SCRIPTPATH}/easybashgui"

sourceFuncs
#testMenuDep
menuMain;

# Reminder, curly braces {} keep current shell context
# while paraentheses () spawn a new subshell
