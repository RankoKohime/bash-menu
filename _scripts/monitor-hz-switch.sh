#!/bin/bash
set -x
# v 1.0

# This script switches the refresh rate on the PG279Q monitor, from 60 (referenced as 59.95) to 165 and back.
# This is done because Nvidia likes to bump up the clock speed when running at the higher rate, even when idle

# Logic

# 1. Query for the available displays.
# 2. Determine which displays are active, and which are disabled, ignore disabled.
# 3. For active displays, gather the current port, resolution, and refresh rate.
# 4. Use if tree to determine if the rate should be increased or decreased.
# 5. Fudge detection to recognize 59.95 because stupid reasons.

# Variables
NOTIFY_SEND=`which notify-send`
ASUS_STRIX_LAPTOP="AU Optronics Corporation"
    ASUS_STRIX_LAPTOP_HZ="120"
    ASUS_STRIX_LAPTOP_REZ="1920x1080"
ASUS_PG279Q="Ancor Communications Inc ROG PG279Q"
    ASUS_PG279Q_HZ="144"
    ASUS_PG279Q_REZ="2560x1440"

# Let's determine what DISPLAYS there actually are.
nvidia-xconfig --query-gpu-info
xrandr --query

DISPLAY=:0.0
OUTPUT=`xrandr -d $DISPLAY|awk '/ connected/{print $1}'`


# Scrub variables
# This is where we'll store the variables that we've determined need to be used in this session.
SCRUB_NAME=
SCRUB_REZ=
SCRUB_HZ=

# This function is called from the window manager hotkey
toggle()
(
  if [[ `xrandr -d $DISPLAY|grep $SCRUB_REZ` == *"59.95*"* || `xrandr -d $DISPLAY|grep $SCRUB_REZ` == *"60.00*"* ]]
    then
      xrandr -d $DISPLAY --output $OUTPUT --mode SCRUB_REZ --rate $SCRUB_HZ
      DISPLAY=$DISPLAY $NOTIFY_SEND "$SCRUB_NAME" "Monitor is now set to $SCRUB_HZ hz"
  fi
)

# This function is called from a cron job that runs once per hour,
# and if the user has been idle for >15 minutes, it bumps the rate down

drop()
(
  if [[ $(DISPLAY=$DISPLAY xprintidle) -gt 550000 && $(xrandr -d $DISPLAY|awk '/$SCRUB_REZ/{while (!match ($0,"59.95*+")) getline; print $2; exit;}') != "59.95*+" ]]
    then
      xrandr -d $DISPLAY --output $OUTPUT --mode $SCRUB_REZ --rate $SCRUB_HZ
      notify-send "$SCRUB_NAME" "Monitor was automatically set to 60hz due to user inactivity"
  fi
)

case $1 in
    toggle)toggle;;
      drop)drop;;
         *)echo "No valid command specified";
           notify-send "PG279Q: Error" "No valid command specified";;
esac
